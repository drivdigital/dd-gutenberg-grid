=== Driv Digital - Grid (gutenberg block) ===
Contributors: drivdigital
Tags: translation
Requires at least: 4.9.8
Tested up to: 5.0.2
Requires PHP: 7.0
Stable tag: 0.2.4

To create a grid follow these steps.

1. Create a new grid block.
2. Select how many columns the grid should have.
3. Add as many grid elements as you need.
4. Adjust the spans slider for any grid element that should span more than one column.

The columns and spans of grids and grid elements can be adjusted later by clicking on the block header (where it says "# column grid" or "Element") and editing the block settings.

== Screenshots ==

1. A grid example
2. Illustration of the resulting layout.
3. Illustration of a 3 column grid.
4. Columns slider for grid.
5. Spans slider for grid elements.

