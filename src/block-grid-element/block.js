/**
 * BLOCK: Grid element
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { RichText, InnerBlocks, InspectorControls } = wp.editor;
const { Fragment } = wp.element;
const { PanelBody, RangeControl } = wp.components;
const { select } = wp.data;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'drivdigital/block-grid-element', {
	title: __( 'Grid element' ),
	icon: 'columns',
	category: 'layout',
	parent: ['drivdigital/block-grid'],
	keywords: [],
	attributes: {
		spans: {
			type: 'number',
			default: 1,
		},
		width: {
			type: 'number',
			default: 12,
		},
	},


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function( props ) {
		const { className, attributes, setAttributes, clientId } = props;
		let editor = select( 'core/editor' );
		let parentId = editor.getBlockRootClientId( clientId );
		let block = editor.getBlocksByClientId( parentId );
		let max = 5;
		if ( block.length ) {
			block[0].attributes.columns;
		}
		let setWidth = ( nextSpans ) => {
			let width = 12;
			block = editor.getBlocksByClientId( parentId );
			if ( block.length ) {
				width = 12 * nextSpans / block[0].attributes.columns;
			}
			setAttributes( {
				spans: nextSpans,
				width: width,
			} );
		};
		setWidth( attributes.spans );
		return (
			<Fragment>
				<InspectorControls>
					<PanelBody>
						<RangeControl
							label={ __( 'Spans' ) }
							value={ attributes.spans }
							onChange={ setWidth }
							min={ 1 }
							max={ max }
						/>
					</PanelBody>
				</InspectorControls>
				<div className={ className }>
					<div className="grid-element__label">Element <em>spans { attributes.spans }</em></div>
					<InnerBlocks/>
				</div>
			</Fragment>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( { attributes } ) {
		return (
			<div className={ `grid-spans-${ attributes.spans } col-sm-${ attributes.width }` }>
				<InnerBlocks.Content/>
			</div>
		);
	},
    deprecated: [
        {
			attributes: {
				spans: {
					type: 'number',
					default: 1,
				},
			},
			migrate: function( { spans } ) {
				return {
					spans: spans,
					width: spans * 4,
				};
			},
            save: function( { attributes } ) {
				return (
					<div className={ `grid-spans-${ attributes.spans }` }>
						<InnerBlocks.Content/>
					</div>
				);
            },
        }
    ]
} );
