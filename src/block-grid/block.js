/**
 * BLOCK: Grid
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { RichText, InnerBlocks, InspectorControls } = wp.editor;
const { Fragment } = wp.element;
const { PanelBody, RangeControl } = wp.components;
const { select } = wp.data;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'drivdigital/block-grid', {
	title: __( 'Grid' ),
	icon: 'columns',
	category: 'layout',
	keywords: [],
	attributes: {
		columns: {
			type: 'number',
			default: 2,
		},
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function( props ) {
		let editor = select( 'core/editor' );
		let { className, attributes, setAttributes, clientId } = props;
		let setCols = ( nextColumns ) => {
			let block = editor.getBlocksByClientId( clientId );
			// Loop through all the children and update their widths
			for(let i=0; i<block[0].innerBlocks.length; i++) {
				let child = block[0].innerBlocks[i];
				if ( ! child || !child.attributes ) {
					continue;
				}
				let spans = child.attributes.spans;
				let width = 12;
				if ( nextColumns > 0 ) {
					width = 12 * spans / nextColumns;
				}
				child.attributes.width = width;
			}
			setAttributes( {
				columns: nextColumns,
			} );
		};
		return (
			<Fragment>
				<InspectorControls>
					<PanelBody>
						<RangeControl
							label={ __( 'Columns' ) }
							value={ attributes.columns }
							onChange={ setCols }
							min={ 2 }
							max={ 6 }
						/>
					</PanelBody>
				</InspectorControls>
				<div className={ className }>
					<div className="grid__label">{ attributes.columns } column grid </div>
					<InnerBlocks allowedBlocks={ [ 'drivdigital/block-grid-element', 'core/block' ] } />
				</div>
			</Fragment>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( { attributes } ) {
		return (
			<div className={ `grid-cols-${ attributes.columns } row` }>
				<InnerBlocks.Content/>
			</div>
		);
	},
	deprecated: [
		{
			attributes: {
				columns: {
					type: 'number',
					default: 2,
				},
			},
			save: function( { attributes } ) {
				return (
					<div className={ `grid-cols-${ attributes.columns }` }>
						<InnerBlocks.Content/>
					</div>
				);
			},
		},
	]
} );
