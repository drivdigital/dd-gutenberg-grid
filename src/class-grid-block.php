<?php
/**
 * Grid block
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @package dd-gutenberg-grid
 */

namespace Driv_Digital;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Grid Block
 */
class Grid_Block {

	const ID = 'grid-block';

	/**
	 * Setup
	 */
	public static function setup() {
		// Enqueue Gutenberg block assets for both frontend + backend.
		add_action( 'wp_enqueue_scripts', __CLASS__ . '::assets' );
		// Enqueue Gutenberg block assets for backend.
		add_action( 'enqueue_block_editor_assets', __CLASS__ . '::backend_assets' );
	}

	/**
	 * Enqueue Gutenberg block assets for both frontend + backend.
	 */
	public static function assets() {
		// Styles.
		wp_enqueue_style(
			self::ID . '_accordion-cgb-style-css',
			plugins_url( 'dist/blocks.style.build.css', __DIR__ )
		);
	}

	/**
	 * Enqueue Gutenberg block assets for backend editor.
	 */
	public static function backend_assets() {
		// Scripts.
		wp_enqueue_script(
			self::ID . '_accordion-cgb-block-js',
			plugins_url( '/dist/blocks.build.js', __DIR__ ),
			array( 'wp-blocks', 'wp-i18n', 'wp-element' ),
			false,
			true
		);
		// Styles.
		wp_enqueue_style(
			self::ID . '_accordion-cgb-block-editor-css',
			plugins_url( 'dist/blocks.editor.build.css', __DIR__ ),
			array( 'wp-edit-blocks' )
		);
	}
}
