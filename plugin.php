<?php
/**
 * Plugin Name: Driv Digital - Gutenberg grid
 * Plugin URI: https://driv.digital/
 * Description: Grid block for gutenberg
 * Author: Driv Digital
 * Author URI: https://driv.digital/
 * Version: 0.2.4
 * Text Domain: dd-gutenberg-grid
 * Domain Path: /languages
 *
 * @package dd-gutenberg-grid
 */

namespace Driv_Digital;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/class-grid-block.php';
Grid_Block::setup();
